import os
import sys
import yaml
import subprocess
from PyQt5.QtWidgets import QApplication, QSystemTrayIcon, QMenu, QAction, QMessageBox
from PyQt5.QtGui import QIcon, QPixmap, QImage
from PyQt5.QtCore import QCommandLineParser, QCommandLineOption

class SystemTrayIcon(QSystemTrayIcon):

    def __init__(self, icon, config, parent=None):
        super(SystemTrayIcon, self).__init__(icon, parent)

        self.parent = parent
        self.dynamic_actions = []

        self.menu = QMenu(parent)

        self.process_config(config)

        self.exit_action = QAction("Exit", parent)
        self.exit_action.triggered.connect(self.exit)
        self.menu.addAction(self.exit_action)

        self.setContextMenu(self.menu)

    def process_config(self, config: list) -> None:
        """Create all actions for the menu.

        :param list: The list of actions with the commands to be executed.
        """
        for action in config:
            self.create_action(action["label"], action["args"])

    def create_action(self, label: str, action: list) -> None:
        """Create a new action.

        :param label: The label in the menu
        :param action: The command separated into an array
        """
        action_obj = QAction(label, self.parent)
        action_obj.triggered.connect(lambda: self.dynamic_action(action))
        self.dynamic_actions.append(action_obj)
        self.menu.addAction(action_obj)

    def dynamic_action(self, action: list) -> None:
        """Run dynamic action.

        :param action: The action to be executed.
        """
        subprocess.Popen(action)

    def exit(self):
        reply = QMessageBox.question(None, 'Stop cluster', 'Are you sure, exiting will end everything in the container!', QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
        if reply == QMessageBox.Yes:
            exit(0)

if __name__ == "__main__":
    app = QApplication(sys.argv)

    app.setQuitOnLastWindowClosed(False)

    parser = QCommandLineParser()
    parser.addHelpOption()
    parser.addOption(
        QCommandLineOption('config', 'Configuration file path', 'CONFIG')
    )
    parser.addPositionalArgument(
        'ICON', 'Systray Icon Path'
    )
    parser.addOption(
        QCommandLineOption('app_name', 'Name for app in systray', 'NAME', "Cluster Core Systray Menu")
    )
    parser.process(app)
    app.setApplicationName(parser.value("app_name"))

    pos_args = parser.positionalArguments()
    if len(pos_args) != 1:
        raise RuntimeError('Expecting exactly one positional argument, the icon file path!')

    config = {}
    if parser.isSet("config"):
        config_path = parser.value("config")
        try:
            with open(config_path) as config_file:
                config = yaml.safe_load(config_file.read())
        except:
            print(f"Error: Could not read config file: {config_path}")


    with open(pos_args[0], 'rb') as icon_file:
        icon = QIcon(
            QPixmap.fromImage(
                QImage.fromData(icon_file.read())
            )
        )

    tray_icon = SystemTrayIcon(icon, config)
    tray_icon.show()

    sys.exit(app.exec_())
