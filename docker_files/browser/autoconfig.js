// Any comment. You must start the file with a comment!

pref("general.config.filename", "firefox.cfg");
pref("general.config.obscure_value", 0);

// Trust local certificates.
pref('security.enterprise_roots.enabled', true);

// Dark Theme
pref('extensions.activeThemeID', "firefox-compact-dark@mozilla.org");

// Don't show 'know your rights' on first run
pref("browser.rights.3.shown", false);
pref("browser.tabs.firefox-view", false);

// Don't show WhatsNew on first run after every update
pref("browser.startup.homepage_override.mstone","ignore");

// Disable the internal PDF viewer
pref("pdfjs.disabled", true);

pref("browser.aboutConfig.showWarning", false);

// Disable the flash to javascript converter
pref("shumway.disabled", true);

// Don't ask to install the Flash plugin
pref("plugins.notifyMissingFlash", false);

// Do not track
pref("privacy.donottrackheader.enabled", true);
pref("privacy.trackingprotection.socialtracking.enabled", true);


//Telemetry shit
pref("browser.newtabpage.activity-stream.telemetry", false);
pref("browser.newtabpage.activity-stream.telemetry.structuredIngestion.endpoint", "");
pref("browser.ping-centre.telemetry", false);
pref("browser.tabs.crashReporting.sendReport", false);
pref("toolkit.telemetry.bhrPing.enabled", false);
pref("toolkit.telemetry.cachedClientID", "");
pref("toolkit.telemetry.shutdownPingSender.enabled", false);
pref("toolkit.telemetry.updatePing.enabled", false);
pref("toolkit.telemetry.cachedClientID", "");
pref("toolkit.telemetry.server", "127.0.0.1");

//Normandy (what ever that is)
pref("app.normandy.enabled", false);

//OpenH264 Cisco Plugin
pref("media.gmp-gmpopenh264.enabled", false);
pref("media.gmp-gmpopenh264.autoupdate", false);
pref("media.navigator.mediadatadecoder_h264_enabled", false);
pref("media.webrtc.hw.h264.enabled", false);

// Search
pref("browser.urlbar.placeholderName", "DuckDuckGo");
pref("browser.urlbar.suggest.topsites", false);
pref("browser.urlbar.suggest.searches", false);
pref("browser.urlbar.suggest.openpage", false);
pref("browser.urlbar.speculativeConnect.enabled", false);

// Startup Homepage
pref("browser.startup.homepage", "about:blank");
pref("browser.newtabpage.enabled", false);

// Fix for crashing tabs (disable multiprocess windows)
pref("browser.tabs.remote.autostart", false);

//Remove default sites
pref("browser.newtabpage.activity-stream.default.sites", "");

