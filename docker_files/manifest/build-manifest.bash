#!/bin/bash
set -e

MANIFEST_DIR=$(dirname $BASH_SOURCE)

unset http_proxy
unset https_proxy


cd $MANIFEST_DIR

if [ -z "$DOMAIN" ]; then
    echo "\$DOMAIN not set"
    exit 1
fi

if [ -z "$MANIFEST_OUTPUT_DIR" ]; then
    echo "\$MANIFEST_OUTPUT_DIR not set"
    exit 1
fi

MANIFEST_OUTPUT_DIR=$(realpath $MANIFEST_OUTPUT_DIR)
mkdir -p $MANIFEST_OUTPUT_DIR


if [ -z "$CERTMANAGER_TAG" ]; then
    export CERTMANAGER_TAG=$(../tool_install/github-get-tag cert-manager cert-manager)
fi

if [ -z "$FLUX_TAG" ]; then
    export FLUX_TAG=$(../tool_install/github-get-tag fluxcd flux2)
    export FLUX_VERSION=$(printf "$FLUX_TAG" | grep -oe "[0-9.]*" |  tr -d '\0')
fi

if [ -z "$TRUSTMANAGER_TAG" ]; then
    export TRUSTMANAGER_TAG=$(../tool_install/github-get-tag cert-manager trust-manager)
fi

if [ -z "$CSI_DRIVER_TAG" ]; then
    export CSI_DRIVER_TAG=$(../tool_install/github-get-tag cert-manager csi-driver)
fi

if [ -z "$COREDNS_TAG" ]; then
    export COREDNS_TAG=$(../tool_install/github-get-tag coredns helm)
    export COREDNS_VERSION=$(printf "$COREDNS_TAG" | grep -oe "[0-9.]*" |  tr -d '\0')
fi

if [ -z "$KYVERNO_TAG" ]; then
    export KYVERNO_TAG=$(../tool_install/github-get-tag kyverno kyverno)
fi


if [ -z "$KEYCLOAK_TAG" ]; then
    export KEYCLOAK_TAG=$(../tool_install/github-get-tag keycloak keycloak)
fi


DNS_DOMAINS=$(echo $DNS_DOMAINS | yq -Po yaml | sed 's/^/    /' | envsubst)
SERVER_MANIFEST_PATH="/var/lib/rancher/k3s/server/manifests"

# curl -Ls https://github.com/cert-manager/cert-manager/releases/download/$CERTMANAGER_TAG/cert-manager.yaml \
#    -o $MANIFEST_OUTPUT_DIR/cert-manager-crds.yaml

for file in *.yaml; do
    cat "$file" | envsubst > $MANIFEST_OUTPUT_DIR/$file
done

cd $MANIFEST_OUTPUT_DIR
for file in *.yaml; do
    printf " -v $MANIFEST_OUTPUT_DIR/$file:$SERVER_MANIFEST_PATH/$file\@server:0 "
done


#curl https://raw.githubusercontent.com/keycloak/keycloak-k8s-resources/$KEYCLOAK_TAG/kubernetes/keycloaks.k8s.keycloak.org-v1.yml > keycloak-crds.yaml
#curl https://raw.githubusercontent.com/keycloak/keycloak-k8s-resources/$KEYCLOAK_TAG/kubernetes/keycloakrealmimports.k8s.keycloak.org-v1.yml > keycloak-realmimports.yaml
#curl https://raw.githubusercontent.com/keycloak/keycloak-k8s-resources/$KEYCLOAK_TAG/kubernetes/kubernetes.yml > keycloak-operator.yaml
