export PATH=$PATH:~/.local/bin

# Enable completions
source <(flux completion bash)
source <(helm completion bash)
source <(k3d completion bash)
source <(k9s completion bash)
source <(kubectl completion bash)
[ -f ~/.kubectl_aliases ] && source ~/.kubectl_aliases
export PATH="${KREW_ROOT:-$HOME/.krew}/bin:$PATH"

source <(kustomize completion bash)
source <(docker completion bash)

COLOR_ORANGE='\[\e[38;5;208m\]'
COLOR_YELLOW='\[\033[1;33m\]'
COLOR_RESET='\[\033[00m\]'
COLOR_GREEN='\[\033[01;32m\]'

PS1="${COLOR_ORANGE}\u${COLOR_RESET}${COLOR_GREEN}☢ ${COLOR_YELLOW}\h${COLOR_RESET}:${COLOR_GREEN}\w${COLOR_RESET}\n$ "
