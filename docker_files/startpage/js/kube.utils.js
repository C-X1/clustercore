function getOrCreateElement(id, parent, tag, classes="", attributes=null, innertext="", innerhtml="") {

  let parent_element = parent
  if (typeof(parent) == "string") {
    parent_element = document.getElementById(parent);
  }

  const existing = document.getElementById(id);
  if (existing) {
    return existing;
  }

  const element = document.createElement(tag);
  element.id = id;

  if (classes) {
    classes.split(" ").forEach((cls) => {
      element.classList.add(cls);
    });
  }

  if (attributes) {
    for (let key in attributes) { element.setAttribute(key, attributes[key]) }
  }

  if (innertext) {
    element.innerText = innertext
  }

  if (innerhtml) {
    element.innerHTML = innerhtml
  }

  parent_element.appendChild(element);
  return element;
}

function getOrCreateAccordionItem(id, parentid, acc_parent) {
  const headerid = id + "-header"
  const buttonid = id + "-button"
  const collapseid = id + "-collapse"
  const bodyid = id + "-body"

  const accordion = getOrCreateElement(parentid, acc_parent, "div", "accordion");
  const item = getOrCreateElement(id, accordion, "div", "accordion-item");
  const header = getOrCreateElement(headerid, item, "h4", "accordion-header");
  const collapse = getOrCreateElement(collapseid, item, "div", "accordion-collapse collapse");
  const body = getOrCreateElement(bodyid, collapse, "div", "accordion-body");
  const button = getOrCreateElement(buttonid, header, "button", "accordion-button collapsed", {
    "data-bs-toggle": "collapse",
    "data-bs-target": "#" + collapseid,
    "aria-controls": collapseid
  });

  return {
    "button": button,
    "body": body
  }
}

/* Example
let acc_parent = document.getElementsByTagName("body")[0];
let a1 = getOrCreateAccordionItem("ai1", "acc1", acc_parent);
let a2 = getOrCreateAccordionItem("ai2", "acc1", acc_parent);

a1.button.innerHTML = "A1";
a2.button.innerHTML = "A2";

a1.body.innerHTML = "A1 BODY";
a2.body.innerHTML = "A2 BODY";
*/
