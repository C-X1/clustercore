function gotoCollapsible(targetId) {
  // Open collapsible
  let collapsible = document.getElementById(targetId);

  if (! collapsible.classList.contains("show")) {
    new bootstrap.Collapse(collapsible);
  }

  const start = Date.now();
  const interval = setInterval(() => {
    if (!collapsible.classList.contains("collapsing")) {
      clearInterval(interval);
      window.scrollTo({
        top: collapsible.getBoundingClientRect().top + window.scrollY - 50,
        behavior: "smooth"
      });
    }
    if (Date.now() - start > timeout) {
      clearInterval(interval);
    }
  }, 10);
}

function sleep(s) {
    return new Promise(resolve => setTimeout(resolve, s * 1000));
}

async function kube_frame_error() {
  console.log("ERROR ClusterRay not found!");
  console.log('Retry after 5 seconds');
  setTimeout(function() {
    kube = document.getElementById("kube")
    kube.data = kube.data;
  }, 5000);
}

function showStartup() {
  const params = new URLSearchParams(window.location.search);
  const startup = params.get('startup');


  if (startup == "true") {
    console.log("startup!");
  }
}
showStartup();
