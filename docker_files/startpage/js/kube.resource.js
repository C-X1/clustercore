
function resourceTypeId(api, resource) {
  return "kr-" + api + "-" + resource;
}

function resourceId(api, resource, name, namespace) {
  let resource_id = resourceTypeId(api, resource);
  if (namespace) {
    resource_id += "-" + namespace;
  }
  resource_id += "-" + name;

  return resource_id;
}


function getResourceTableBody(api, resource, namespaced) {
  const resourceid = resourceTypeId(api, resource);
  const acc_parent = document.getElementById("resource-view");

  const accordion_item = getOrCreateAccordionItem(
    resourceid,
    "resource-view-accordion",
    acc_parent
  );

  accordion_item.button.innerHTML = api + "/" + resource;

  const tableid = resourceid + "-table";
  const tableheadid = tableid + "-head";
  const tableheadrowid = tableid + "-head-row";
  const tablebodyid = tableid + "-body";

  const table = getOrCreateElement(tableid, accordion_item.body, "table", "table");
  const tablehead = getOrCreateElement(tableheadid, table, "thead");
  const tableheadrow = getOrCreateElement(
    tableheadrowid, tablehead, "tr");

  if (! tableheadrow.innerHTML) {
    let namespaceheader = "";
    if (namespaced) {
      namespaceheader = "<th>Namespace</th>";
    }
    tableheadrow.innerHTML = "<th>Name</th>" +
                             namespaceheader;
  }

  return getOrCreateElement(tablebodyid, table, "tbody");
}



function setOrCreateCollapsibleTableRow(id, tablebody, values, detailFunction) {
  const tablerow = getOrCreateElement(id, tablebody, "tr");
  const tablerow_collapsible = getOrCreateElement(id, tablebody, "tr");

  console.log(values);
  for (i in values) {
    getOrCreateElement(id + "-" + i.toString(), tablerow, "td").innerText = values[i];
  }
}

function updateResourceView(resources) {
  resources.forEach((resource) => {
    if (resource.items.length > 0) {
      const tablebody = getResourceTableBody(
        resource.origin.endpoint.api,
        resource.origin.endpoint.resource,
        resource.origin.endpoint.namespaced
      );

      resource.items.forEach((item) => {
        console.log(item);

        const resource_id = resourceId(
          resource.origin.endpoint.api,
          resource.origin.endpoint.resource,
          item.metadata.name,
          item.metadata.namespace)

        let values = [];
        values.push(item.metadata.name);
        if (resource.origin.endpoint.namespaced) {
          values.push(item.metadata.namespace);
        }

        setOrCreateCollapsibleTableRow(resource_id, tablebody, values, null);
      });
    }
  });
}
