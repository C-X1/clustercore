var update_interval = 5000;
var update_timeout = null;
var page = "dashboard";
var resources = [];
var namespace_filter = "";
var resource_filter = "";
var resource_name_filter = ".*";

function processApis(items) {
  let endpoints = [];
  items.forEach((item) => {
    endpoints = endpoints.concat(processVersions(item.versions, item.metadata.name));
  });
  return endpoints;
}

function processVersions(versions, api) {
  let endpoints = [];
  versions.forEach((version) => {
    endpoints = endpoints.concat(processResources(version.resources, api, version.version));
  });
  return endpoints;
}

function processResources(resources, api, version) {
  let endpoints = [];
  resources.forEach((resource) => {
    let namespace_template = "/";
    let namespaced = resource.scope == "Namespaced";

    if (resource.verbs.includes("get")) {
      if (namespaced && namespace_filter) {
        namespace_template = "/namespaces/{namespace}/";
      }

      let api_string = resource.responseKind.group;
      if (! api_string) {
        api_string = api;
      }

      endpoints.push({
        "api": api,
        "version": version,
        "resource": resource.resource,
        "namespaced": namespaced,
        "url": "apis/" + api_string + "/" + version + namespace_template + resource.resource
      })
    }
  });
  return endpoints;
}

async function getNamespaces() {
  try {
    const response = await fetch("/kube/api/v1/namespaces");
    const data = await response.json();
    return data.items.map((namespace) => namespace.metadata.name);
  } catch (error) {
    console.error('Error:', error);
    return [];
  }
}

async function getEndpoints() {
  try {
    const response = await fetch("/kube/apis", {
      headers: {
        Accept: "application/json;g=apidiscovery.k8s.io;v=v2;as=APIGroupDiscoveryList,application/json;g=apidiscovery.k8s.io;v=v2beta1;as=APIGroupDiscoveryList,application/json"
      }
    });
    const data = await response.json();
    return processApis(data.items)
  } catch (error) {
    console.error('Error:', error);
    return [];
  }
}

function createUrls(endpoints, namespaces) {
  let urls = [];
  endpoints.forEach((endpoint) => {
    if(endpoint.namespaced) {
      namespaces.forEach((namespace) => {
        urls.push(
          {
            "endpoint": endpoint,
            "namespace": namespace,
            "url": endpoint.url.replace(/\{namespace\}/g, namespace)
          }
        )
      });
    } else {
      urls.push(
        {
          "endpoint": endpoint,
          "url": endpoint.url
        }
      )
    }
  });
  return urls;
}

async function getItems(url) {
  try {
    const response = await fetch("/kube/" + url);
    const data = await response.json()
    return {
      "items": data.items || [],
      "code": response.status,
      "msg": response.statusText
    }
  } catch (error) {
    console.error('Error:', error);
    return [];
  }
}

async function getResources(urls_data) {
  const promises = urls_data.map(async function(url_data) {
    const response = await getItems(url_data.url);
    const data = await response;
    return {
      items: data.items,
      origin: url_data,
      code: data.code,
      msg: data.msg
    };
  });

  const collected_items = await Promise.all(promises);
  return collected_items;
}

function applyFilters() {
  namespace_filter = document.getElementById('filterNamespace').value || "^$|.*";
  resource_filter = document.getElementById('filterResource').value || ".*";
  resource_name_filter = document.getElementById('filterResourceName').value || ".*";
}

function filterArrayByField(arr, field, regex) {
  return arr.filter(item => {
    const value = getFieldByPath(item, field);
    return regex.test(value);
  });
}

function getFieldByPath(obj, path) {
  const fields = path.split('.');
  let value = obj;
  for (let field of fields) {
    if (value && value.hasOwnProperty(field)) {
      value = value[field];
    } else {
      return "";
    }
  }
  return value;
}

function clearUpdateInterval() {
  if (update_timeout != null) {
    clearTimeout(update_timeout);
    update_timeout = null;
  }
}

function setUpdateInterval() {
  if (document.getElementById("btnAutoUpdate").checked)
  {
    update_interval = parseInt(document.getElementById("updateInterval").value);
    update_timeout = setTimeout(update, update_interval * 1000);
  } else {
    clearTimeout(update_timeout);
    update_timeout = null;
    update_interval = 0; //Off
  }
}

function filterResources(resources) {
  let resources_f = JSON.parse(JSON.stringify(resources));

  resources_f = filterArrayByField(resources_f, "origin.endpoint.resource", new RegExp(resource_filter));
  resources_f = filterArrayByField(resources_f, "origin.namespace", new RegExp(namespace_filter));
  resources_f.forEach((resource) => {
    resource.items = filterArrayByField(resource.items || [], "metadata.name", new RegExp(resource_name_filter));
  });

  return resources_f;
}


async function update() {
  clearUpdateInterval();

  const endpoints = await getEndpoints();
  const namespaces = await getNamespaces();
  let urls_data = createUrls(endpoints, namespaces);

  if (resource_filter) {
    urls_data = filterArrayByField(urls_data, "endpoint.resource", new RegExp(resource_filter));
  }

  if (namespace_filter) {
    urls_data = filterArrayByField(urls_data, "namespace", new RegExp(namespace_filter));
  }

  resources = await getResources(urls_data);

  let resources_f = filterResources(resources)

  updateResourceView(resources);

  //console.log(resources_f)
  //console.log(resources_f.map(obj => obj.items).flat());

  setUpdateInterval();
}
update();
