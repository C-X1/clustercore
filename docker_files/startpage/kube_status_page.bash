#!/bin/bash

SCRIPT_DIR=$(dirname $BASH_SOURCE)
kubectl proxy \
    --address 0.0.0.0 \
    --port 18125 \
    --reject-methods='POST,PUT,PATCH,DELETE' \
    --accept-hosts="127\.0\.0\.1" \
    --reject-paths='^/api/.*/pods/.*/exec,^/api/.*/pods/.*/attach,.*/secrets($|/)' \
    --www "$SCRIPT_DIR" \
    --www-prefix="/" \
    --api-prefix="/kube"
