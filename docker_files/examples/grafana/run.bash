#!/bin/bash
SCRIPT_DIR=$(dirname $BASH_SOURCE)

helm upgrade -i grafana -n default oci://registry-1.docker.io/bitnamicharts/grafana --values $SCRIPT_DIR/values.yaml --wait

firefox-esr --new-tab grafana.clustercore.internal
