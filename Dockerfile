FROM docker:dind as INSTALL
ARG TOOL_INSTALL_DIR=/opt/kube

RUN apk add --no-cache \
    bash \
    bash-completion \
    bind-tools \
    breeze \
    curl \
    ca-certificates \
    dnsmasq \
    firefox-esr \
    font-noto-emoji \
    gettext \
    icu-data-full\
    inotify-tools-libs \
    jq \
    kate \
    kdialog \
    konsole \
    ncurses \
    oxygen \
    p11-kit-trust \
    python3 \
    py3-qt5 \
    py3-yaml \
    sudo \
    ttf-liberation \
    ttf-droid \
    xclip \
    yq

COPY docker_files /clustercore

RUN mkdir -p $TOOL_INSTALL_DIR \
    && find /clustercore/tool_install/ -name "install_*" | HOME=/clustercore/tpl_host/home bash

# Firefox setup
RUN mkdir -p /usr/lib/firefox-esr/browser/defaults/preferences/ \
    && mkdir -p /usr/lib/firefox-esr/distribution/ \
    && mv /clustercore/browser/autoconfig.js /usr/lib/firefox-esr/defaults/pref/ \
    && mv /clustercore/browser/policies.json /usr/lib/firefox-esr/distribution/ \
    && rm /clustercore/browser -rf

# Get bootstrap
RUN mkdir bootstrap \
    && cd bootstrap \
    && export TOOL_INSTALL_DIR=$(pwd) \
    && /clustercore/tool_install/github-install twbs bootstrap dist.zip "-o bootstrap.zip" "v5.3.3" \
    && unzip bootstrap.zip \
    && mv bootstrap-* bootstrap \
    && mv bootstrap/css/bootstrap.min.css /clustercore/startpage/css \
    && mv bootstrap/js/bootstrap.bundle.min.js /clustercore/startpage/js \
    && cd .. \
    && rm bootstrap -rf

# Get jQuery
RUN curl https://code.jquery.com/jquery-3.7.1.js > /clustercore/startpage/js/jquery.js

# Get ttf fonts
RUN mkdir -p /clustercore/startpage/ttf \
 && curl https://raw.githubusercontent.com/google/fonts/main/ofl/k2d/K2D-Regular.ttf > /clustercore/startpage/ttf/K2D-Regular.ttf \
 && curl https://raw.githubusercontent.com/google/fonts/main/ofl/k2d/K2D-ExtraBold.ttf > /clustercore/startpage/ttf/K2D-ExtraBold.ttf

ENV INSTALLER_MODE=PRINT

#COPY README.md /README.md
COPY entry /entry
COPY install /install

RUN cd /clustercore/tpl_host/home \
 && wget https://raw.githubusercontent.com/ahmetb/kubectl-aliases/master/.kubectl_aliases \
 && cp /clustercore/tpl_host/home /home/clustercore -R
