# ClusterCore (WIP)

Your cluster containers in a docker in a docker in a cri-io with extendible systray menu launcher, most used kubernetes tools and more...

*This is madness!* 
*No! This is clustercore!*

Also with selfsigned certs accepted by the browser in the docker...

And a dashboard - (not ready yet)

## Build

# Deprecated docker build

```bash
docker build . --tag clustercore
```

# Kaniko

TODO


## Installation for Users

Depending on how the image was tagged like for example `clustercore.myregistry.company.com` 
`clustercore` must be replaced in the image variable with that tag.

``` bash
IMAGE="clustercore" bash -c 'bash -c "$(docker run -e DISPLAY --rm $IMAGE /install)"'
```
